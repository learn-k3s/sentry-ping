package garden.bots.sentryping

import io.netty.handler.codec.mqtt.MqttQoS
import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.core.buffer.Buffer
import io.vertx.mqtt.MqttClient
import io.vertx.mqtt.MqttClient.create
import io.vertx.mqtt.MqttServerOptions

class MainVerticle : AbstractVerticle() {

  override fun start(startPromise: Promise<Void>) {

    val options = MqttServerOptions()
    options.setPort(1883)
    options.setHost("0.0.0.0")
    //options.setHost("192.168.64.17")
    //options.setHost("sentry.master.demo.k33g")

    val client: MqttClient = create(vertx)

    client.connect(options.port, options.host) { asyncResult ->
      when {
        asyncResult.failed() -> {
          client.disconnect()
        }
        asyncResult.succeeded() -> {
          println("🎉")
          client.publish("/weather", Buffer.buffer("🌤"), MqttQoS.AT_LEAST_ONCE, false, false)
          client.subscribe("/hey", 2) // 2 == qos - QoS level
          //client.disconnect()
        }
      }
    }
  }
}
